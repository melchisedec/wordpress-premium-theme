<?php 
	
	/*
		This is the template for the sidebar
		
		@package Prof theme
	*/

if ( ! is_active_sidebar( 'sunset-sidebar' ) ) {
	return;
}

?>

<aside id="secondary" class="widget-area" role="complementary">

							<?php
								wp_nav_menu( array(
									'theme_location' => 'primary',
									'container' => false,
									'menu_class' => 'nav navbar-nav',
									'walker' => new Prof_Walker_Nav_Primary()
								) );	
							?>
	<?php dynamic_sidebar( 'sunset-sidebar' ); ?>
	
</aside>