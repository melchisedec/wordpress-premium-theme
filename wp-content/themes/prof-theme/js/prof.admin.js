jQuery( document ).ready( function( $ ){
	//alert('me');
	var mediaUploader;
	$( '#upload-button' ).on( 'click', function( e ){
		e.preventDefault();
		if( mediaUploader )
		{
			mediaUploader.open();
			return;
		}
		mediaUploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose a Profile Picture',
			button: {
				text : 'Choose picture'
			},
			multiple:false
		});

		mediaUploader.on( 'select', function(){
			attachment = mediaUploader.state().get( 'selection' ).first().toJSON();
			$( '#profile_pic' ).val( attachment.url ); // id for hidden input
			$( '#profile-picture-preview' ).css( 'background-image', 'url(' + attachment.url + ')' );
		});
		
		mediaUploader.open();
	}); 




	$( '#remove-picture' ).on( 'click', function( e ){
		e.preventDefault();
		var answer = confirm( "Are you sure you want to delete the profile picture?" );
		if( answer == true ) 
		{
			$( '#profile_pic' ).val( '' );
			$( '.prof-general-form' ).submit();

			
		} else
		{
			
		}
		
		return;
	});
});