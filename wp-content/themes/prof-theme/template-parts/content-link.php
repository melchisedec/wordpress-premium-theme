<?php
/*
	@package Prof Theme
	=========================================
				LINK Post Format
	=========================================
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'prof-format-link' ); ?>>
	<header class="entry-header text-center">
		<?php 
			$link = prof_grab_url();
			the_title( '<h1 class="entry-title"><a href="' . $link . '" target="">', '<div class="link-icon"><span class="sunset-icon sunset-link"></span></div></a></h1>' ); ?>

	</header>
	
	
</article>