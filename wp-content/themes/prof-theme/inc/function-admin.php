<?php
/*
	=========================================
					ADMIN PAGE
	=========================================
*/

	function prof_add_admin_page()
	{
		
		//dashicons-palmtree
		// Generate Prof Admin page
		add_menu_page( 'Prof Theme Options', 'Prof theme', 'manage_options', 'prof', 'prof_theme_create_page', get_template_directory_uri() . '/images/affroapps.png', 110 );

		// Generate Prof Admin sub page
		add_submenu_page( 'prof', 'Prof Settings', 'Sidebar' , 'manage_options' , 'prof', 'prof_theme_create_page' );

		add_submenu_page( 'prof', 'Prof Theme Options', 'Theme Options' , 'manage_options' , 'prof_theme', 'prof_theme_support_page' );
		//>>> add_submenu_page( 'prof', $title, $menutitle, $capability , $idFROMadd_settings_section$pageName , $callbackFn );
		add_submenu_page( 'prof', 'Prof Contact Form', 'Contact Form' , 'manage_options' , 'prof_theme_contact', 'prof_theme_contact_page' );


		add_submenu_page( 'prof', 'Prof CSS Options', 'Custom CSS' , 'manage_options' , 'prof_css', 'prof_theme_settings_page' );

		

		// Activate custom settings
		add_action( 'admin_init', 'prof_custom_settings' );
	}
	add_action( 'admin_menu', 'prof_add_admin_page' );

	function prof_custom_settings()
	{
		// SIDEBAR Settings api fields are used
		register_setting( 'prof-settings-group', 'profile_pic' );
		register_setting( 'prof-settings-group', 'first_name' );
		register_setting( 'prof-settings-group', 'last_name' );
		register_setting( 'prof-settings-group', 'user_description' );
		register_setting( 'prof-settings-group', 'twitter_handle' );
		register_setting( 'prof-settings-group', 'facebook_handle' );
		register_setting( 'prof-settings-group', 'gplus_handle' );



		add_settings_section( 'prof-sidebar-options', 'Sidebar Options', 'prof_sidebar_options', 'prof' );
		add_settings_field( 'sidebar-profile-picture', 'profile Picture', 'prof_profile_pic', 'prof', 'prof-sidebar-options' );
		add_settings_field( 'sidebar-name', 'Full name', 'prof_sidebar_name', 'prof', 'prof-sidebar-options' );
		add_settings_field( 'sidebar-name-description', 'User name description', 'prof_user_description', 'prof', 'prof-sidebar-options' );

		add_settings_field( 'sidebar-twitter', 'Twitter handler', 'prof_sidebar_twitter', 'prof', 'prof-sidebar-options', 'prof_sanitize_twitter_handle' );
		add_settings_field( 'sidebar-facebook', 'Facebook handler', 'prof_sidebar_facebook', 'prof', 'prof-sidebar-options' );
		add_settings_field( 'sidebar-gplus', 'Google plus handler', 'prof_sidebar_gplus', 'prof', 'prof-sidebar-options' );

		// THEME SUPPORT Settings api fields are used
		// register_setting( 'prof-theme-support', 'post_formats', 'prof_post_formats_callback' );		
		register_setting( 'prof-theme-support', 'post_formats' );	
		register_setting( 'prof-theme-support', 'custom_header' );	
		register_setting( 'prof-theme-support', 'custom_background' );

		add_settings_section( 'prof-theme-options', 'Theme Options', 'prof_theme_options_cf', 'prof_theme' );
		add_settings_field( 'post-formats', 'Post Formarts', 'prof_post_formats', 'prof_theme', 'prof-theme-options' );
		add_settings_field( 'custom-header', 'Custom Header', 'prof_custom_header', 'prof_theme', 'prof-theme-options' );
		add_settings_field( 'custom-background', 'Custom Background', 'prof_custom_background', 'prof_theme', 'prof-theme-options' );

		

		// COntact form options
		//>>> register_setting( $groupName(to be referenced in form's settings_fields), $settingName );
		register_setting( 'prof-contact-options', 'activate_contact' );
		//>>> add_settings_section( string $id, string $title,string $callback, string $pageToBeReferncedinForm's do_settings_sections );
		add_settings_section( 'prof-contact-section', 'Contact Form', 'prof_contact_section', 'prof_theme_contact' );

		//>>> add_settings_field( string $id, string $title, string $callback, string $pageFromAdSubMenu(2nd last arg), string $section, array $args(optional to remove) );
		add_settings_field( 'activate-form', 'Activate Contact Form', 'prof_contact_cf','prof_theme_contact', 'prof-contact-section' );



		// COntact CSS options
		//>>> register_setting( $groupName(to be referenced in form's settings_fields), $settingName );
		register_setting( 'prof-custom-css-options', 'prof_custom_css', 'prof_sanitize_custom_css' );

		//>>> add_settings_section( string $id, string $title,string $callback, string $pageToBeReferncedinForm's do_settings_sections );
		add_settings_section( 'prof-custom-css-section', 'Custom CSS', 'prof_custom_css_section_callback', 'prof_css' );

		//>>> add_settings_field( string $id, string $title, string $callback, string $pageFromAdSubMenu(2nd last arg), string $section, array $args(optional to remove) );
		add_settings_field( 'custom-css', 'Your custom css', 'prof_custom_css_callback','prof_css', 'prof-custom-css-section' );


	}

	//POST FORMATS FUNCTION

	// function prof_post_formats_callback( $input )
	// {
	// 	return $input;
	// }

	//CUSTOM CSS

	function prof_custom_css_section_callback()
	{
		echo "Customize them with your styles";

	}

	function prof_custom_css_callback()
	{
		$css = get_option( 'prof_custom_css' );
		$css = ( empty( $css) ? '/* Sunset theme Custom CSS */' : $css );
		echo '<div id="customCss" placeholder="CUSTOMIZE CSS" >' .$css. '</div><textarea id="prof_custom_css" name="prof_custom_css" style="display:none;visibility:hidden">'. $css .'</textarea>';

	}

	//POST FORMATS FUNCTION

	function prof_theme_options_cf()
	{
		echo 'Activate and Deactivate specific Theme Support options';
	}

	function prof_contact_section()
	{
		echo 'Activate and Deactivate built in contact form';
	}

	function prof_contact_cf()
	{
		$options = get_option( 'activate_contact' );
		$checked = ( @$options == 1 ? 'checked' : '' );
		echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '.$checked.' /></label><br>';
	}

	function prof_post_formats()
	{
		$options = get_option( 'post_formats' );
		$formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
		$output = '';
		foreach( $formats as $format )
		{
			$checked = ( @$options[$format] == 1 ? 'checked' : '' );
			$output .= '<label><input type="checkbox" id="'.$format.'" name="post_formats['.$format.']" value="1" '.$checked.' />'.$format.'</label><br>';
		}
		echo $output;
	}

	function prof_custom_header()
	{
		$options = get_option( 'custom_header' );
		$checked = ( @$options == 1 ? 'checked' : '' );
		echo '<label><input type="checkbox" id="custom_header" name="custom_header" value="1" '.$checked.' />Activate custom header</label><br>';
		
	}

	function prof_custom_background()
	{
		$options = get_option( 'custom_background' );
		$checked = ( @$options == 1 ? 'checked' : '' );
		echo '<label><input type="checkbox" id="custom_background" name="custom_background" value="1" '.$checked.' />Activate custom Background</label><br>';
		
	}

	// SIDBAR OPTIONS FORMAT
	function prof_sidebar_options ()
	{
		echo 'Customize your side bar info';
	}

	function prof_profile_pic()
	{
		$Picture = esc_attr( get_option( 'profile_pic' ) );
		if( empty( $Picture ) )
		{
			echo '<input type="button" class="button button-secondary" id="upload-button" value="Upload profile pic" /><input type="hidden" id="profile_pic" name="profile_pic" value="" />';

		}
		else
		{
			echo '<input type="button" class="button button-secondary" id="upload-button" value="Upload profile pic" /><input type="hidden" id="profile_pic" name="profile_pic" value="'. $Picture .'" />
			<input type="button" class="button button-secondary" value="Remove" id="remove-picture">';

		}
		
	}

	function prof_sidebar_name()
	{
		$firstName = esc_attr( get_option( 'first_name' ) );
		$lastName = esc_attr( get_option( 'last_name' ) );
		echo '<input type="text" name="first_name" value="'. $firstName .'" placeholder = "First Name" />';
		echo '<input type="text" name="last_name" value="'. $lastName .'" placeholder = "Last Name" />';
	}

	function prof_sidebar_twitter()
	{
		$twitterHandle = esc_attr( get_option( 'twitter_handle' ) );
		echo '<input type="text" name="twitter_handle" value="'. $twitterHandle .'" placeholder = "Twitter" /><p class = "description">Input your username without the @ symbol</p>';
	}

	function prof_user_description()
	{
		$userDescription = esc_attr( get_option( 'user_description' ) );
		echo '<input type="text" name="user_description" value="'. $userDescription .'" placeholder = "User description" /><p class="description">Write something smart</p>';
	}

	function prof_sidebar_facebook()
	{
		$facebookHandle = esc_attr( get_option( 'facebook_handle' ) );
		echo '<input type="text" name="facebook_handle" value="'. $facebookHandle .'" placeholder = "Facebook" />';
	}

	function prof_sidebar_gplus()
	{
		$gplusHandle = esc_attr( get_option( 'gplus_handle' ) );
		echo '<input type="text" name="gplus_handle" value="'. $gplusHandle .'" placeholder = "Google plus" />';
	}

	// Sanitiztion settings // lesson 5 sanitization did not work

	function prof_sanitize_twitter_handle( $input )
	{
		//echo 'Youre InPUT'. $input;
		$output = sanitize_text_field( $input );
		$output = str_replace( '@', '', $output );
		return $output;
	}

	function prof_sanitize_custom_css( $input )
	{
		$output = esc_textarea( $input );
		return $output;
	}

	

	// Template submenu functions
	function prof_theme_support_page()
	{
		require_once( get_template_directory() . '/inc/templates/prof-theme-support.php' );

	}


	function prof_theme_create_page()
	{
		// generation of admin page
		//echo "<h1>Prof theme General pages</h1>";
		require_once( get_template_directory() . '/inc/templates/prof-admin.php' );
	}
	

	function prof_theme_contact_page()
	{
		// generation of admin page
		//echo "<h1>Prof theme General pages</h1>";
		require_once( get_template_directory() . '/inc/templates/prof-contact.php' );
	}


	function prof_theme_settings_page()
	{
		// generation of sub menu page
		//echo "<h1>Prof theme CSS pages</h1>";
		require_once( get_template_directory() . '/inc/templates/prof-custom-css.php' );

	}