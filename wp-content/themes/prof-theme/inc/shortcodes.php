<?php
/*
	@package Prof theme
	===============================
			SHORTCODE OPTIONS
	===============================
*/
function prof_tooltip( $atts, $content = null )
{
	// get the attributes
	$atts = shortcode_atts(
		array(
			'placement' => 'top',
			'title' 	=> '',
		),
		$atts,
		'tooltip'
	);

	$title = ( $atts[ 'title' ] == '' ? $content : $atts[ 'title' ] );

	// return HTML  
	return '<span class="prof-tooltip" data-toggle="tooltip" data-placement="' . $atts[ 'placement' ] . '" title="' . $title . '">' . $content . '</span>';

}
add_shortcode( 'tooltip', 'prof_tooltip' );

/*
	[tooltip placement="top" title="Thi is the title"]This is the content[/tooltip]
*/


function prof_popover( $atts, $content = null )
{

	/*[popover data-toggle="popover" title="Pop over title" placement="top" trigger="click" content="This is popover content" >This is the clickable content[/popover]*/

/*<button type="button" class="btn btn-lg btn-danger" data-toggle="popover" data-trigger="click" data-html="" title="Pop over title" data-content="And her's amzing content">Click to toggle</button>*/

	// get the attributes
	$atts = shortcode_atts(
		array(
			'placement' => 'top',
			'title' 	=> '',
			'trigger' 	=> 'click',
			'popover'	=> '',
			'content'	=> ''
		),
		$atts,
		'popover'
	);

	// return HTML  
	return '<span class="prof-popover" data-toggle="popover" data-placement="' . $atts[ 'placement' ] . '" title="' . $atts[ 'title' ] . '" data-trigger="' . $atts[ 'trigger' ] . '" data-content="' . $atts[ 'content' ] . '">' . $content . '</span>';

}
add_shortcode( 'popover', 'prof_popover' );






/*
	Contact form shortcode
	[contact_form]
*/

function prof_contact_form( $atts, $content = null )
{
	// get the attributes
	$atts = shortcode_atts(
		array(),
		$atts,
		'contact_form'
	);



	// return HTML  
	ob_start();  // output buffering
	include 'templates/contact-form.php';
	return ob_get_clean();

}
add_shortcode( 'contact_form', 'prof_contact_form' );



