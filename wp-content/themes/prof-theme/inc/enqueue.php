 <?php
/*
	=========================================
			BACK END ADMIN ENQUEUE FUNCTIONS
	=========================================
*/

function prof_load_admin_scripts( $hook )
{
	// echo $hook;
	// die();

	if( 'toplevel_page_prof' == $hook) {

		wp_register_style( 'prof_admin', get_template_directory_uri() . '/css/prof.admin.css', array(), '1.0.0', 'all' );
		wp_enqueue_style( 'prof_admin' );

		wp_enqueue_media();

		wp_register_script( 'prof-admin-script', get_template_directory_uri() . '/js/prof.admin.js', array( 'jquery' ), '1.0.0', true );
		wp_enqueue_script( 'prof-admin-script' );
		// 
	} else if ( 'prof-theme_page_prof_css' == $hook ) 
	{
		wp_register_style( 'ace', get_template_directory_uri() . '/css/prof.ace.css', array(), '1.0.0', 'all' );
		wp_enqueue_style( 'ace' );


		//wp_enqueue_style( 'ace', get_template_directory_uri() . '/css/prof.ace.css' , array(), '1.0.0', 'all' );
		wp_enqueue_script( 'prof_ace', get_template_directory_uri() . '/js/ace/ace.js', array( 'jquery'), '1.2.1', true );
		wp_enqueue_script( 'prof-custom-css-script', get_template_directory_uri() . '/js/prof.custom_css.js', array( 'jquery' ), '1.0.0', true );

	} else {  return;  }

}
add_action( 'admin_enqueue_scripts', 'prof_load_admin_scripts' );


/*
	=========================================
			FRONT END ADMIN ENQUEUE FUNCTIONS
	=========================================
*/

	function prof_load_scriptis()
	{
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6', 'all' );
		wp_enqueue_style( 'prof', get_template_directory_uri() . '/css/prof.css', array(), '1.0.0', 'all' );
		wp_enqueue_style( 'raleway', '<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">' );

		//wp_deregister_script( 'jquery' );
		wp_register( 'jquery' , get_template_directory_uri() . '/js/jquery.js', false, '1.11.3', true );

		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery'), '3.3.6', true );
		wp_enqueue_script( 'prof', get_template_directory_uri() . '/js/prof.js', array( 'jquery'), '1.0.0', true );
 
	}
	add_action( 'wp_enqueue_scripts', 'prof_load_scriptis' );