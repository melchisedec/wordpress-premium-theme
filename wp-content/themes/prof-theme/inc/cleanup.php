 <?php
/*
	@package Prof Theme
	=========================================
			REMOVE GENERATOR VERSION NUMBER
	=========================================
*/
	/* remover version string from js and css */

	function prof_remove_wp_version_strings( $src )
	{
		global $wp_version;
		parse_str( parse_url( $src, PHP_URL_QUERY ), $query ); //$query is an array

		if(	!empty( $query[ 'ver' ] ) && $query[ 'ver' ] === $wp_version )
		{
			$src = remove_query_arg( 'ver', $src );
		}
		return $src;
	}
	add_filter( 'script_loader_src', 'prof_remove_wp_version_strings' );
	add_filter( 'style_loader_src', 'prof_remove_wp_version_strings' );

	/* remove metatag generator from header */

	function prof_remover_meta_version()
	{
		return '';
	}
	add_filter( 'the_generator', 'prof_remover_meta_version' );
	// video 16 20:26
