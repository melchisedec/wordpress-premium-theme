<?php
	/*
		@package Prof Theme
		=========================================
					WIDGET CLASS
		=========================================
	*/

class Prof_Profile_Widget extends WP_Widget
{
	// Setup the widget name, description, et.c
	public function __construct()
	{
		$widget_ops = array(
			'classname'		=>	'prof-profile-widget',
			'description'	=>	'Custom Prof profile Widegt',
		);
		parent::__construct( 'prof_profile', 'Prof Profile', $widget_ops  );
	}

	// BACK END DISPLAY OF WIDGET
	public function form( $instance )
	{
		echo "<p>No options for this widget</p><p>You can control the fields of this widget.<a href='./wp-admin/edit.php?post_type=prof-contact'>HERE</a></p>";

	}

	// FRONT END DISPLAY OF WIDGET
	public function widget( $args, $instance )
	{

		$picture = esc_attr( get_option( 'profile_pic' ) );
		$firstName = esc_attr( get_option( 'first_name' ) );
		$lastName = esc_attr( get_option( 'last_name' ) );
		$fullname = $firstName . ' ' . $lastName;
		$userDescription = esc_attr( get_option( 'user_description' ) );

		$twitter_icon = esc_attr( get_option( 'twitter_handler' ) );
		$facebook_icon = esc_attr( get_option( 'facebook_handler' ) );
		$gplus_icon = esc_attr( get_option( 'gplus_handler' ) );

		echo $args[ 'before_widget' ];
		?>
	<div class="text-center">
		<div class="image-container">
			<div id="profile-picture-preview" class="profile-picture" style="background-image: url(<?= $picture ?>);"></div>
		</div>
		<h1 class="prof-username"> <?= $fullname ?> </h1>
		<h2 class="prof-description"> <?= $userDescription ?></h2>
		<div class="icons-wrapper">
			<?php if( !empty( $twitter_icon ) ): ?>
				<a href="https://twitter.com/<?php echo $twitter_icon; ?>" target="_blank"><span class="sunset-icon-sidebar sunset-icon sunset-twitter"></span></a>
			<?php endif; 
			if( !empty( $gplus_icon ) ): ?>
				<a href="https://plus.google.com/u/0/+<?php echo $facebook_icon; ?>" target="_blank"><span class="sunset-icon-sidebar sunset-icon sunset-googleplus"></span></a>
			<?php endif; 
			if( !empty( $facebook_icon ) ): ?>
				<a href="https://twitter.com/<?php echo $gplus_icon; ?>" target="_blank"><span class="sunset-icon-sidebar sunset-icon sunset-facebook"></span></a>
			<?php endif; ?>
		</div>
	</div>

		<?php

		echo $args[ 'after_widget' ];

		return ;

	}
}

add_action( 'widgets_init', function(){
	register_widget( 'Prof_Profile_Widget' );
} );

/*
	Edit default WP widgets
*/

	function prof_tag_cloud_font_change( $args ){
		$args[ 'smallest' ] = 8;
		$args[ 'largest' ] = 8;

		return $args;
	}

	add_filter( 'widget_tag_cloud_args', 'prof_tag_cloud_font_change' );

/*
	Save Posts views
*/

function prof_save_post_views( $postID ){

	$metaKey = 'prof_post_views';
	$views = get_post_meta( $postID, $metaKey, true );

	$count = ( empty( $views ) ? 0 : $views );
	$count++;

	update_post_meta( $postID, $metaKey, $count );

	echo '<h1>' . $views . '</h1>';

}

remove_action( 'wp_head', 'ajacent_posts_rel_link_wp_head', 10, 0 );

/*
	Popular Posts widgets
*/

class Prof_Popular_Posts_Widget extends WP_Widget
{
	// Setup the widget name, description, et.c
	public function __construct()
	{
		$widget_ops = array(
			'classname'		=>	'prof-popular-posts-widget',
			'description'	=>	'Popular Posts Widegt',
		);
		parent::__construct( 'prof_popular_posts', 'Prof Popular Posts', $widget_ops  );
	}

	// backend display of widget
	public function form( $instance )
	{
		$title = ( !empty( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Popular Posts'  );
		$tot = ( !empty( $instance[ 'tot' ] ) ? absint( $instance[ 'tot' ] ) : 4 );

		$output = '<p>';
		$output .= '<label for="' . esc_attr( $this->get_field_id( 'title' ) ) . '">Title:</label>';
		$output .= '<input type="text" class="widefat" id="' . esc_attr( $this->get_field_id( 'title' ) ) . '" name="' . esc_attr( $this->get_field_name( 'title' ) )  . '" value="' . esc_attr( $title )  . '"';
		$output .= '<p>';

		$output .= '<p>';
		$output .= '<label for="' . esc_attr( $this->get_field_id( 'tot' ) ) . '">number of Posts:</label>';
		$output .= '<input type="number" class="widefat" id="' . esc_attr( $this->get_field_id( 'tot' ) ) . '" name="' . esc_attr( $this->get_field_name( 'tot' ) )  . '" value="' . esc_attr( $tot )  . '"';
		$output .= '<p>';

		echo $output;
	}

	// update widget
	public function update( $new_instance, $old_instance )
	{
		$instance = [];
		$instance[ 'title' ] = ( ! empty( $new_instance[ 'title' ] ) ? strip_tags( $new_instance[ 'title' ] ) : '' );
		$instance[ 'tot' ] = ( ! empty( $new_instance[ 'tot' ] ) ? absint( strip_tags( $new_instance[ 'tot' ] ) ) : 0 );

		return $instance;

	}

	// FRONT END display of widget

	public function widget( $args, $instance )
	{
		$tot = absint( $instance[ 'tot' ] );
		$posts_args = [
			'post_type'			=>	'post',
			'posts_per_page'	=>	$tot,
			'meta_key'			=>	'prof_post_views',
			'orderby'			=>	'meta_value_num',
			'order'				=>	'DESC'
		];

		$posts_query = new WP_Query( $posts_args );

		echo $args[ 'before_widget' ];

		if( ! empty( $instance[ 'title' ] ) ) :
			echo $args[ 'before_title' ] . apply_filters( 'widget_title', $instance[ 'title' ] )  . $args[ 'after_title' ];
		endif;

		if( $posts_query->have_posts() ):
			//echo "<ul>";
				while( $posts_query->have_posts() ): $posts_query->the_post();
					echo '<div class="media">';
					echo '<div class="media-left"><img class="media-object" src="' . get_template_directory_uri() . '/images/post-' . ( get_post_format() ? get_post_format() : 'standard') . '.png" alt="' . get_the_title() . '"/></div>';
					echo '<div class="media-body">';
					echo '<a href="' . get_the_permalink() . '" title="' . get_the_title() . '">' . get_the_title() . '</a>';
					echo '<div class="row"><div class="col-xs-12">'. prof_posted_footer( true ) .'</div></div>';
					echo '</div>';
					echo '</div>';
				endwhile;

			//echo "</ul>";
		endif;

		echo $args[ 'after_widget' ];
	}


}

add_action( 'widgets_init', function(){
	register_widget( 'Prof_Popular_Posts_Widget' );
});




