<h1>Prof Sidebar Options</h1>

<?php settings_errors(); ?>

<?php

	$picture = esc_attr( get_option( 'profile_pic' ) );
	$firstName = esc_attr( get_option( 'first_name' ) );
	$lastName = esc_attr( get_option( 'last_name' ) );
	$fullname = $firstName . ' ' . $lastName;
	$userDescription = esc_attr( get_option( 'user_description' ) );

	$twitter_icon = esc_attr( get_option( 'twitter_handler' ) );
	$facebook_icon = esc_attr( get_option( 'facebook_handler' ) );
	$gplus_icon = esc_attr( get_option( 'gplus_handler' ) );

?>

<di v class="prof-sidebar-preview">
	<div class="prof-sidebar">
		<div class="image-container">
			<div id="profile-picture-preview" class="profile-picture" style="background-image: url(<?= $picture ?>);"></div>
		</div>
		<h1 class="prof-username"> <?= $fullname ?> </h1>
		<h2 class="prof-description"> <?= $userDescription ?></h2>
		<div class="icons-wrapper">
			<?php if( !empty( $twitter_icon ) ): ?>
				<span class="sunset-icon-sidebar dashicons-before dashicons-twitter"></span>
			<?php endif; 
			if( !empty( $gplus_icon ) ): ?>
				<span class="sunset-icon-sidebar sunset-icon-sidebar--gplus dashicons-before dashicons-googleplus"></span>
			<?php endif; 
			if( !empty( $facebook_icon ) ): ?>
				<span class="sunset-icon-sidebar dashicons-before dashicons-facebook-alt"></span>
			<?php endif; ?>
		</div>

	</div>
</div>
<form method="post" action="options.php" class="prof-general-form">
	<?php settings_fields( 'prof-settings-group' ); ?>
	<?php do_settings_sections( 'prof' ); ?>
	<?php submit_button( 'Save Changes', 'primary', 'btnSubmit' ); ?>
</form>
<?php // bloginfo( 'name' ); ?>

