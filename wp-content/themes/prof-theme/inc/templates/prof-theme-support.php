<h1> Prof Theme Support </h1>
<?php settings_errors(); ?>

<form method="post" action="options.php" class="prof-general-form">
	<?php settings_fields( 'prof-theme-support' ); ?>
	<?php do_settings_sections( 'prof_theme' ); ?>
	<?php submit_button(); ?>
</form>