<h1> Prof Contact Form </h1>

<?php settings_errors(); ?>
<p>Use this shortcode ro activate the Contact Form inside a page or a post </p>
<p><code>[contact_form]</code></p>

<form method="post" action="options.php" class="prof-general-form">
	<?php settings_fields( 'prof-contact-options' ); ?>
	<?php do_settings_sections( 'prof_theme_contact' ); ?>
	<?php submit_button(); ?>
</form>