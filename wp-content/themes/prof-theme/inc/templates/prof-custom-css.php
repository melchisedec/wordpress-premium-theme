<h1> Prof Custom CSS </h1>

<?php settings_errors(); ?>

<form id="save-custom-css-form" method="post" action="options.php" class="prof-general-form">
	<?php settings_fields( 'prof-custom-css-options' ); ?>
	<?php do_settings_sections( 'prof_css' ); ?>
	<?php submit_button(); ?>
</form>