
<form id="profContactForm" action="#" method="post" data-url="<?php echo admin_url( 'admin-ajax.php' ); ?>" class="prof-contact-form">
	<div class="form-group">
		<input type="text" class="form-control prof-form-control" name="name" id="name" placeholder="Your name"  required="required" >
		<small class="text-danger form-control-msg">Your name is required</small>
	</div>

	<div class="form-group">
		<input type="email" class="form-control prof-form-control" name="email" id="email" placeholder="Your email"  required="required" >
		<small class="text-danger form-control-msg">Your Email is required</small>

	</div>

	<div class="form-group">
		<textarea  class="form-control prof-form-control" name="message" id="message" placeholder="Your message"  required="required" ></textarea> 
		<small class="text-danger form-control-msg">Message  is required</small>

	</div>
	<div class="text-center">
		<button type="submit" class="btn btn-default btn-lg btn-prof-form">Submit</button>
		<small class="text-info form-control-msg js-form-submission"> Submission in process, please wait..</small>
		<small class="text-info form-control-msg js-form-success"> message succefully submited</small>
		<small class="text-info form-control-msg js-form-error"> Error occured.. please try again</small>
	</div>


</form>