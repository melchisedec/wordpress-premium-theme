<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'melik');

/** MySQL database password */
define('DB_PASSWORD', 'melik');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z11ehDJulOt?p`Fhl:sZ)O*])#=+q74&sj<Afe2<No2+GJ`HTI+%A+DCJ#y2#eZ2');
define('SECURE_AUTH_KEY',  'bP;Hd4zCB1aQW/C`R(nVN}-`o5([t~2.UX.%_4-bzFN/@xD e)<~TErTM<)1XIZw');
define('LOGGED_IN_KEY',    'mi[Olz.TNtO<yrHTw%#ar7$h&,RR&1(x{VdP}-sc&*+`6.G<IxI7/>:=}D@C(gSx');
define('NONCE_KEY',        '27?*%+pUAf?A0N:r[wa502mW:3$PF4 ]I(=T|LVS^i]$MvHuB|)7u/BXC=`HJR[0');
define('AUTH_SALT',        '|7,BYoQQa_k=PdCc14^~z8Gd`x~AxR5bCGscf~)!;!xxz=]=YMS9%(QMgAr(XeM[');
define('SECURE_AUTH_SALT', 'd<^a.*uyK?cbU4XjATZhz/wldF22eJo;8UhuW@:>gK@= Wz%bKh(bg7<6@TS^Yg#');
define('LOGGED_IN_SALT',   '>Bqxk_``fS=.+; [D=PHLTHXa<@h6&.qU?&vsd(g][(4)~Nv=e(3JBB6cYQlEV}k');
define('NONCE_SALT',       'qV910eaYLU/3PZj$EJ[]3X/!Pwd>DC$WWBqxMs-]Ih0~*0r]TPB*}Dg#N`CsLl[<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_theme_dev_sun_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
